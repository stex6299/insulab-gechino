#!/usr/bin/env python3

import serial
from serial import *

# Definisco la porta seriale e le impostazioni
ser = serial.Serial("/dev/ttyUSB0", baudrate = 115200, bytesize = EIGHTBITS, stopbits = STOPBITS_ONE, parity = PARITY_NONE, timeout = .2)

# Apro la porta se non gia' aperta
if not ser.isOpen():
        ser.open()

# Entriamo in modalita' macchina
ser.write("AT+MACHINE\r\n".encode())



print("\n*** Welcome to status.py :) ***")






ser.write("AT+GET,231\r\n".encode())
tmp = ser.read(100).decode()
print("ADC readback voltage: " + str(tmp).replace("OK", "").replace("=", "").strip() + " V")


ser.write("AT+GET,232\r\n".encode())
tmp = ser.read(100).decode()
print("ADC readback current: " + str(tmp).replace("OK", "").replace("=", "").strip() + " mA")
