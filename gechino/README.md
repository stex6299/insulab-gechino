# What's here
In this folder scripts for controlling `CAEN DT5485P` (frienldy called *gechino*) can be found.  
To run all of this scripts, the `pyserial` module should be installed (`pip install pyserial`)

- [accendi.py](./accendi.py) allows to set the bias and turn on the module. The reference bias can be set either in the code (e.g. `voltage = 27.0` and calling as `./accendi.py`, or specifying the tension as first command line argument (e.g. `./accendi.py 28.5`)
- [spegni.py](./spegni.py) turns off the bias
- [status.py](./status.py) reads **current** and **tension**
- [GUI.py](./GUI.py) runs a Graphical User Interface (GUI) for turning bias on and off, setting the reference voltage and reading current and tension. In the future I will update with an auto refresh for the read values.

The performance of this scripts has been improved by setting the timeout to 200 ms.


