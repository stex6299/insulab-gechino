#!/usr/bin/env python3

import serial
from serial import *
# Definisco la porta seriale e le impostazioni
ser = serial.Serial("/dev/ttyUSB0", baudrate = 115200, bytesize = EIGHTBITS, stopbits = STOPBITS_ONE, parity = PARITY_NONE, timeout = .2)

# Apro la porta se non gia' aperta
if not ser.isOpen():
        ser.open()

# Entriamo in modalita' macchina
ser.write("AT+MACHINE\r\n".encode())


# Spengo
print("I'm going to disable the output")

ser.write("AT+SET,0,0\r\n".encode())
ser.write("AT+GET,0\r\n".encode())
tmp = ser.read(100).decode()



# Leggo dopo aver spento
ser.write("AT+GET,231\r\n".encode())
tmp = ser.read(100).decode()
#print("La tensione che mi dice lui vale " + str(tmp))
print("ADC readback voltage: " + str(tmp).replace("OK", "").replace("=", "").strip() + " V")
