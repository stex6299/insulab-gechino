#!/usr/bin/env python3

import serial
import sys
from serial import *

# Definisco la porta seriale e le impostazioni
ser = serial.Serial("/dev/ttyUSB0", baudrate = 115200, bytesize = EIGHTBITS, stopbits = STOPBITS_ONE, parity = PARITY_NONE, timeout = .2)

# Apro la porta se non gia' aperta
if not ser.isOpen():
        ser.open()

# Entriamo in modalita' macchina
ser.write("AT+MACHINE\r\n".encode())

# Scelgo la tensione
if len(sys.argv) > 1:
	#voltage = f"{float(sys.argv[1]):.1f}"
	voltage = sys.argv[1]
else:
	voltage = 27.0
print("I'm setting the voltage to " + str(voltage) + "V")

# Setto la tensione
strToWrite = "AT+SET,2," + str(voltage) + "\r\n"
ser.write(strToWrite.encode())

ser.write("AT+GET,2\r\n".encode())
tmp = ser.read(100).decode()

print("Voltage set: " + str(tmp).replace("OK", "").replace("=", "").strip() + " V")


# Accendo
print("I'm going to enable the output")

ser.write("AT+SET,0,1\r\n".encode())
ser.write("AT+GET,0\r\n".encode())
tmp = ser.read(100).decode()


# Leggo dopo aver acceso
ser.write("AT+GET,231\r\n".encode())
tmp = ser.read(100).decode()
print("ADC readback voltage: " + str(tmp).replace("OK", "").replace("=", "").strip() + " V")
