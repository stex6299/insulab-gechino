#!/usr/bin/env python3

import tkinter as tk

from PIL import ImageTk, Image
from tkinter import *

import serial
import sys
from serial import *

import inspect



# ***** FUNCTIONS *****
def updateValues(myReadV: str, myReadI: str):
    """
    This function constructs the strings shown in the `Read Values` section
    """
    read_v.set(f"Bias: {myReadV} V")
    read_i.set(f"Current: {myReadI} mA")



# Serial configs
_baudrate = 115200
_bytesize = EIGHTBITS
_stopbits = STOPBITS_ONE
_parity = PARITY_NONE
_timeout = .2

def HV_ON():
    """
    Turn on the bias
    """
    print("Turning bias on...")

    try:
        with serial.Serial(serial_address.get(), baudrate = _baudrate, 
                        bytesize = _bytesize, stopbits = _stopbits, 
                        parity = _parity, timeout = _timeout) as ser:
            
            # Entriamo in modalita' macchina
            ser.write("AT+MACHINE\r\n".encode())

            # Scrivo comando di accensione e leggo
            ser.write("AT+SET,0,1\r\n".encode())

            # Readback (Dovrebbe ritornare ok)
            ser.write("AT+GET,0\r\n".encode())
            tmp = ser.read(100).decode()
            
            print("Done\n")
    except Exception as ex:
        print(f"Oh Oh problems!!")
        print(f"In {inspect.stack()[0][3]}")
        print(f"Caller: {inspect.stack()[1][3]}")
        print(f"Error: {ex}\n")


def HV_OFF():
    """
    Turn off the bias
    """
    print("Turning bias off...")

    try:
        with serial.Serial(serial_address.get(), baudrate = _baudrate, 
                        bytesize = _bytesize, stopbits = _stopbits, 
                        parity = _parity, timeout = _timeout) as ser:
            
            # Entriamo in modalita' macchina
            ser.write("AT+MACHINE\r\n".encode())

            # Scrivo comando di accensione e leggo
            ser.write("AT+SET,0,0\r\n".encode())

            # Readback (Dovrebbe ritornare ok)
            ser.write("AT+GET,0\r\n".encode())
            tmp = ser.read(100).decode()

            print("Done\n")
    except Exception as ex:
        print(f"Oh Oh problems!!")
        print(f"In {inspect.stack()[0][3]}")
        print(f"Caller: {inspect.stack()[1][3]}")
        print(f"Error: {ex}\n")
        

def HV_SET():
    """
    Set the voltage
    """
    print("Setting voltage...")

    try:
        # Setto la tensione
        # Con il cast a float verifico che sia effettivamente un float
        voltage = str(float(set_var.get()))
        print(f"Setting voltage to {voltage} V")


        with serial.Serial(serial_address.get(), baudrate = _baudrate, 
                        bytesize = _bytesize, stopbits = _stopbits, 
                        parity = _parity, timeout = _timeout) as ser:
            
            # Entriamo in modalita' macchina
            ser.write("AT+MACHINE\r\n".encode())

            # Comando per settare la tensione
            strToWrite = "AT+SET,2," + voltage + "\r\n"
            ser.write(strToWrite.encode())

            # Readback
            ser.write("AT+GET,2\r\n".encode())
            tmp = ser.read(100).decode()

            print("Voltage set returned: " + str(tmp).replace("OK", "").replace("=", "").strip() + " V")
            
            print("Done\n")

    except ValueError as ex:
        print("ATTENZIONE !!!")
        print(f"Stai provando a settare {set_var.get()} come tensione ...")

    except Exception as ex:
        print(f"Oh Oh problems!!")
        print(f"In {inspect.stack()[0][3]}")
        print(f"Caller: {inspect.stack()[1][3]}")
        print(f"Error: {ex}\n")

def GET_STATUS():
    """
    Reads current voltage and current
    """
    print("Reading current values...")

    try:
        with serial.Serial(serial_address.get(), baudrate = _baudrate, 
                        bytesize = _bytesize, stopbits = _stopbits, 
                        parity = _parity, timeout = _timeout) as ser:
            
            # Entriamo in modalita' macchina
            ser.write("AT+MACHINE\r\n".encode())

            # Tensione
            ser.write("AT+GET,231\r\n".encode())
            tmpVolt = ser.read(100).decode()
            tmpVolt2 = str(tmpVolt).replace("OK", "").replace("=", "").strip()
            tmpVolt3 = f"{float(tmpVolt2):.2f}"
            print("ADC readback voltage: " + tmpVolt2 + " V")

            # Corrente
            ser.write("AT+GET,232\r\n".encode())
            tmpCurr = ser.read(100).decode()
            tmpCurr2 = str(tmpCurr).replace("OK", "").replace("=", "").strip()
            tmpCurr3 = f"{float(tmpCurr2):.2f}"
            print("ADC readback current: " + tmpCurr2 + " mA")

            # Aggiorno gli oggetti stringvar che la mostrano sul monitor
            updateValues(tmpVolt3, tmpCurr3)
            
            print("Done\n")
    except Exception as ex:
        print(f"Oh Oh problems!!")
        print(f"In {inspect.stack()[0][3]}")
        print(f"Caller: {inspect.stack()[1][3]}")
        print(f"Error: {ex}\n")



# ***** VARIABLES *****
# ...





# ***** GUI *****

window = tk.Tk()
window.title("Gechino")
#window.geometry("550x650")
window.configure(background="white", highlightcolor="orange red", highlightthickness= 10 )


frame1 = tk.Frame(master=window, width=550, height=80, bg = 'orange red')
frame1.pack(fill=tk.X,padx=20, pady=40)


get_label = tk.Label(frame1, text = "Gechino settings", font = ("Montserrat", 20, 'bold'), bg = 'orange red', fg='white' )
get_label.pack(fill=tk.X,)

frame2 = tk.Frame(master=window, width=100, height=100, bg = 'white')
frame2.pack(padx=20, pady=10, fill=tk.X)
frame2.grid_propagate(False)

values_label = tk.Label(frame2, text = "Read values",  font = ("Montserrat", 15), width = 15, bg = 'white')
values_label.grid(row = 0, column = 0, columnspan=2, padx=5, pady=5, sticky=tk.W)

get_button = tk.Button(frame2, text = "Get", command = GET_STATUS,
                       width = 10,  font = ("Montserrat", 11), bg = 'white')
get_button.grid(row= 2, column=0, rowspan=2,  padx=25, pady=5)

# Tensione letta
read_v = tk.StringVar()
read_v.set('666 V')
get_v_label = tk.Label(frame2, textvariable = read_v,  font = ("Montserrat", 11), bg = 'white')
get_v_label.grid(row = 2, column = 1, padx=25, pady=0,sticky=tk.W)

# Corrente letta
read_i = tk.StringVar()
read_i.set('666 mA')
get_i_label = tk.Label(frame2, textvariable= read_i,  font = ("Montserrat", 11), bg = 'white')
get_i_label.grid(row = 3, column = 1, padx=25, pady=0,sticky=tk.W)

serial_label = tk.Label(frame2, text = "Serial adress",  font = ("Montserrat", 11), bg = 'white')
serial_label.grid(row = 2, column = 4, padx=25, pady=5,sticky=tk.W)


# Indirizzo seriale
serial_address = tk.StringVar()
serial_address.set("/dev/ttyUSB0")
serial_entry = tk.Entry(frame2, width = 25, textvariable=serial_address,  font = ("Montserrat", 11))
serial_entry.grid(row= 3, column=4, sticky=tk.E, padx=25, pady=5)


frame3 = tk.Frame(master=window, width=100, height=200, bg = 'white')
frame3.pack(padx=20, pady=10, fill=tk.X)


set_label = tk.Label(frame3, text = 'Bias to set (V)',  font = ("Montserrat", 11), bg = 'white')
set_label.grid(row = 0, column = 0, padx=25, pady=0,sticky=tk.W)

# Tensione da settare
set_var = tk.StringVar()
set_var.set('0')
set_entry = tk.Entry(frame3, textvariable=set_var, width = 12,  font = ("Montserrat", 11), bg = 'white')
set_entry.grid(row= 1, column=0, rowspan=2,  padx=5, pady=5)

set_button = tk.Button(frame3, text = 'Set',  command = HV_SET,
                       font = ("Montserrat", 11), bg = 'white')
set_button.grid(row = 1, column = 1,  padx=15, pady=5)


on_button = tk.Button(frame3, text = 'HV ON',  command = HV_ON, 
                      font = ("Montserrat", 11), bg = 'white', )
on_button.grid(row = 1, column = 2,  padx=15, pady=5)

off_button = tk.Button(frame3, text = 'HV OFF',  command = HV_OFF, 
                       font = ("Montserrat", 11), bg = 'white')
off_button.grid(row = 1, column = 3,  padx=15, pady=5)

status_label = tk.Label(frame3, text = "Status",  font = ("Montserrat", 11), bg = 'white')
status_label.grid(row = 0, column = 4, columnspan = 3, padx=5, pady=5)

myCanvas = tk.Canvas(frame3, width = 20, height = 20, bg = 'white', bd=0)  
myCanvas.grid(row = 1, column = 4, columnspan=3, sticky=tk.E,padx=15, pady=5)
myOval = myCanvas.create_oval(2, 2, 15, 15,)
myCanvas.itemconfig(myOval, fill = "grey")

frame3 = tk.Frame(master=window, width=100, height=200, bg = 'white')
frame3.pack(padx=20, pady=10, fill=tk.X) 

image = PhotoImage(file="./gechino.png", format="png")
img = Label(frame3, image=image)
img.pack()


window.mainloop()


