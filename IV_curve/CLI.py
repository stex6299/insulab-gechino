#!/usr/bin/env python3

import argparse
import globalVars

# Module contains:
# serial_connect, power_on, power_off, power_status, scan
import IV_read_mod
import sys, os, re, glob



parser = argparse.ArgumentParser(description="CLI for simple current reader")
parser.add_argument(
    "--serialport",
    dest="serialport",
    type = str,
    help="OPTIONAL: default is /dev/ttyUSB0",
    default="/dev/ttyUSB0"
)
parser.add_argument(
    "--start",
    dest="start", 
    type=float, 
    help="First voltage value in order to to begin scan (float number)"
)
parser.add_argument(
    "--stop",
    dest="stop", 
    type=float, 
    help="Last voltage value in order to end scan (float number)"
)
parser.add_argument(
    "--step",
    dest="step", 
    type=float, 
    help="Step value between 'start' and 'stop' scan (float number)"
)
parser.add_argument(
    "--filename",
    dest="filename", 
    type=str, 
    help= "The file name used to save and export recorded data (example: filetest00)"
)
parser.add_argument(
    "--plot",
    dest="plot_bool",
    action='store_true',
    help="OPTIONAL: plotting data (--plot to enable, omit to disable)",
)
args = parser.parse_args()
figure, filename = IV_read_mod.scan(args.start, args.stop, args.step, args.filename, args.plot_bool, args.serialport)
