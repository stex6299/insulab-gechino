#!/usr/bin/env python3 

from mainwindow import *
from PyQt5.QtCore import pyqtSlot
# from PyQt5.QtGui import *
# from PyQt5.QtWidgets import *
# from PyQt5.Qt import Qt
from IV_read_mod import *
import threading

import sys, os, re, glob
#from functions import *


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        # self.btnSource.clicked.connect(self.printMe)
    @pyqtSlot()
    def on_btn_scan_clicked(self):
        start_V = float(self.txt_start.text() )
        stop_V = float(self.txt_stop.text() )
        step_V = float(self.txt_step.text() )
        filename = self.txt_filename.text()
        plot_check = bool(self.plot_check.isChecked() )
        serialport = self.txt_serialport.text()
        print(start_V, stop_V, step_V, filename, plot_check)
        scan(start_V, stop_V, step_V, filename, plot_check, serialport)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    app.setApplicationName("IV Reader GUI")

    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())
