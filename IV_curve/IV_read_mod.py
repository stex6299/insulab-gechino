import time, sys, serial
import numpy as np 
import matplotlib.pyplot as plt
from serial import *


# [Now is argument in CLI -> /dev/ttyUSB0 by default]
# SERIAL PORT: EDIT HERE if different
#serialport="/dev/ttyUSB0"

# Define serial port and settings
def serial_connect(serialport):
    ser = serial.Serial(serialport, baudrate = 115200, bytesize = EIGHTBITS, stopbits = STOPBITS_ONE, parity = PARITY_NONE, timeout = .2)
    # Opening port if closed
    if not ser.isOpen():
            ser.open()

    # Machine mode
    ser.write("AT+MACHINE\r\n".encode())
    return ser






## FUNCTION - POWER ON
def power_on(voltage, ser):

    print("POWER ON")
    # Check if voltage is in range, error otherwise
    if (voltage>=20) and (voltage<=60):
        print("  Voltage OK in range (20-60 V)")
        pass
    else:
        print("INCORRECT VOLTAGE (< 20 OR > 60!)")
        return


    # Voltage set
    strToWrite = "AT+SET,2," + str(voltage) + "\r\n"
    ser.write(strToWrite.encode())

    ser.write("AT+GET,2\r\n".encode())
    tmp = ser.read(100).decode()

    # [Only for debug: print data read]
    # print("Voltage set: " + str(tmp).replace("OK", "").replace("=", "").strip() + " V")

    # Power on
    ser.write("AT+SET,0,1\r\n".encode())
    ser.write("AT+GET,0\r\n".encode())
    tmp = ser.read(100).decode()


    # Reading real-time voltage
    ser.write("AT+GET,231\r\n".encode())
    tmp = ser.read(100).decode()
    print("  ADC readback voltage: " + str(tmp).replace("OK", "").replace("=", "").strip() + " V")
    print("  OK")






## FUNCTION - POWER STATUS
def power_status(ser):
    print("READING DATA:")

    # Get voltage from serial port, then print it    
    ser.write("AT+GET,231\r\n".encode())
    tmp = ser.read(100).decode()
    print("  ADC readback voltage: " + str(tmp).replace("OK", "").replace("=", "").strip() + " V")
    # Saving readback value [V], if needed
    voltage = str(tmp).replace("OK", "").replace("=", "").strip()

    # Get current from serial port, then print it
    ser.write("AT+GET,232\r\n".encode())
    tmp = ser.read(100).decode()
    print("  ADC readback current: " + str(tmp).replace("OK", "").replace("=", "").strip() + " mA")
    # Saving readback value [mA], if needed
    current = str(tmp).replace("OK", "").replace("=", "").strip()

    # Confirm operation successful and return [V] and [mA] values
    print("  Read success!\n")    
    return float(voltage), float(current)






## FUNCTION - POWER OFF
def power_off(ser):
    # Shutting down: alert
    print("SHUTTING DOWN")
    # Power off
    ser.write("AT+SET,0,0\r\n".encode())
    ser.write("AT+GET,0\r\n".encode())
    tmp = ser.read(100).decode()

    # Operation completed
    print("...Done!\n")






## FUNCTION - SCAN 
# First, get all necessary values
def scan(start, stop, step, filename, plot_bool, serialport):
    ser = serial_connect(serialport)
    
   
    # Check if start and stop are ok, otherwise exit code
    # [voltage safety thresholds are available also in FUNCTION - POWER ON]
    if (start<20):
        print("STARING POINT IS TOO LOW! (< 20)\nExit...")
        sys.exit()
    elif (stop>60):
        print("VOLTAGE EXCEEDS MAXIMUM VALUE(> 60)\nExit...")
        sys.exit()
    elif (stop-start <=0):
        print("PLEASE CHECK (START [V]) < (STOP [V]) \nExit...")
        sys.exit() 
    else: 
        print("START AND STOP VOLTS ARE OK!")
        pass

    # Initialize 3 vectors (to store recorded data):
    voltage_set = np.arange(start, stop, step)  # voltage set by user
    voltage_measured = np.zeros(len(voltage_set))  # voltage in real-time
    current = np.zeros(len(voltage_set))        # current in real-time


    # IMPORTANT STATEMENT:
    # try:
        # do the scan
    # except: KeyboardInterrupt
        # print emergency power off, then quit 

    # If no Ctrl+C or similar interruptions are engaged, then do the scan regularly
    # Otherwise, any Ctrl+C or similar SIGINT will trigger power off device and exit script
    
    # Case I. No interruptions (all clear, do the scan)
    try:
        # Begin the scan
        for i in range(len(voltage_set)):
            # print round n. [i]
            print("MEASURE N.",i,"--> V=",voltage_set[i],"--------------------------")
            # power on with start_voltage + [i]-step 
            power_on(voltage_set[i], ser)
            # Wait for stabilizing...
            print("  Please wait 3s to stabilize...")
            time.sleep(3)
            # Record the readback voltage and current in real-time
            voltage_measured[i], current[i] = power_status(ser)
            # print end round n. [i], wait little time for next round
            print("++++++++++++++++++++++++++++++++++ END MEASURE N.",i,"\n\n")
            time.sleep(1.5)
        
        # Scan ended, powering off
        power_off(ser)
        time.sleep(1)

        # Saving recorded values in "filename".npz
        print("All data recorded, saving file in directory...")
        save = np.savez_compressed(filename, v_s = voltage_set, v_m=voltage_measured, c=current)
        # All done!
        time.sleep(0.5)
        print("File saved!")
        
        # IF --plot 1, script will add a quick IV plot with recorded data, otherwise no plot shown
        # In the CLI is optional, can be omitted as argument
        if plot_bool:
            plt.style.use("ggplot")
            figure = plt.plot(voltage_measured, current, color="orange", label="Recorded data", linewidth=2)
            plt.title("IV curve - result")
            plt.xlabel('Voltage [V]')
            plt.ylabel('Current [mA]')
            plt.grid(1)
            plt.legend()

            return filename, plt.show()
        # IF no plot, it returns only saved data filename.npz in directory
        else:
            print("Done!\nExit...") 
            figure = None
            return filename, print("Figure:", figure)


    # Case II. When Ctrl+C or similar is detected during execution, for safety reason 
    #          the device MUST BE TURNED OFF
    except (KeyboardInterrupt, SystemExit):
        # Get alert message
        print("\nCtrl+C\n\nSIGINT detected! Wait, powering off...")
        time.sleep(1)
        # Power off, print OK then quit program
        power_off(ser)
        print("Powered off! Quit")
        sys.exit()
